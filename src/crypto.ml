let hmac512 key data =
  let open Digestif.SHA512 in
  hmac_string ~key data |> to_hex

let sha256 hex =
  hex |> Hex_ext.init |> Hex.to_string |> Digestif.SHA256.digest_string
  |> Digestif.SHA256.to_hex

let ripemd160 hex =
  hex |> Hex_ext.init |> Hex.to_string |> Digestif.RMD160.digest_string
  |> Digestif.RMD160.to_hex
