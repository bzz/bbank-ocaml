exception Error
exception InvalidSecretKey

type point = Infinity | Point of Z.t * Z.t 

let inverse (a : Z.t) (p : Z.t) =
  Z.(invert a p)

let int_pow a b = truncate ((float_of_int a) ** (float_of_int b))  


module type FIELD = 
sig
  type t

  val get_field : t -> Z.t
  (** Returns an integer specifying the finite field (i.e. p for the prime field Fp) *)                       
  val get_g : t -> point 
  (** Returns the base point *)
  val get_n : t -> Z.t
  (** Returns the order of the base point *)

  val get_a : t -> Z.t

  val get_b : t -> Z.t    

  val is_point : point -> t -> bool 
  (** Check if a point lies on an eliptic curve *)
  val double_point : point -> t -> point 
  (** Doubles a given point on a given eliptic curve *)
  val add_point : point -> point -> t -> point
  (** Adds two given points on a given eliptic curve *)
  val multiply_point : point -> Z.t -> t -> point 
  (** Scalar multiplication of a point and an integer on an elliptic curve *)
    
  val secp256k1 : t
end

module PrimeField : FIELD = struct
  type t = {
    p : Z.t;
    a : Z.t;
    b : Z.t;
    g : point;
    n : Z.t;
    h : Z.t
  }  

  let get_field curve = curve.p
  let get_g curve = curve.g
  let get_n curve = curve.n
  let get_a curve = curve.a
  let get_b curve = curve.b                   

  (*Elliptic Curve Functions*)

  let normalize (r : point) curve =
    let p = curve.p in
    let normalize_aux (x : Z.t) =
      match x with
      | x when Z.(x < zero) -> Z.(p + x)
      | x -> x
    in
    match r with
    | Infinity -> Infinity
    | Point (x_r, y_r) -> Point (normalize_aux x_r, normalize_aux y_r) 

  let is_point (r : point) curve =
    match r with
    | Infinity -> true
    | Point (x, y) ->
      let a = Z.((y ** 2) mod curve.p) in
      let b = Z.(((x ** 3) + (curve.a * x) + curve.b) mod curve.p) in
      a = b

  let double_point (r : point) curve =
    if not (is_point r curve) then raise Error;
    let p = curve.p in
    match r with
    | Infinity -> Infinity
    | Point (x,y) when y = Z.zero -> Infinity
    | Point (x, y) ->
      let a = Z.(((((~$ 3) * (x ** 2))) + curve.a) mod p) in
      let b = Z.((inverse (y + y) p) mod p) in
      let s = Z.(a * b mod p) in
      let x_r = Z.(((s ** 2) - (x + x)) mod p) in
      let y_r = Z.((-y + (s * (x - x_r))) mod p) in
      normalize (Point (x_r, y_r)) curve

  let add_point (r1 : point) (r2 : point) curve =
    if not ((is_point r1 curve) && (is_point r2 curve)) then raise Error;
    let p = curve.p in
    match r1, r2 with
    | _, Infinity -> r1
    | Infinity, _ -> r2
    | Point (x1, y1), Point (x2, y2) when x1 = x2 -> Infinity
    | r1, r2 when r1 = r2 -> double_point r1 curve
    | Point (x1, y1), Point (x2, y2) ->
      let s = Z.(((y2 - y1) * (inverse (x2 - x1) p)) mod p) in
      let xf = Z.(((s ** 2) - x1 - x2) mod p) in
      let yf = Z.((s * (x1 - xf) - y1) mod p) in
      normalize (Point (xf, yf)) curve

  (* Point multiplication is implemented using the double-and-add algorithm *)
  (*let multiply_point (q : point) (d : Z.t) curve =
    let d_bits = Z.format "%b" d in
    let r = ref q in
      String.iter (fun di -> r := double_point (!r) curve;
                             match di with
                               | '0' -> ()
                               | '1' -> r := add_point (!r) (q) curve
                               | _ -> failwith "Not a bit") d_bits;
      (!r)*)

  (* Point multiplication is implemented using montogomery ladders*)
  let multiply_point q d curve =
    let d_bits = Z.format "%b" d in
    let r_0 = ref Infinity in
    let r_1 = ref q in
    String.iter (fun di ->
        if di = '0' then
          begin
            r_1 := add_point !r_0 !r_1 curve;
            r_0 := double_point !r_0 curve
          end
        else
          begin
            r_0 := add_point !r_0 !r_1 curve;
            r_1 := double_point !r_1 curve
          end) d_bits;
    (!r_0)

  (* ECC data representation functions*)

  let secp256k1 =
    {
      p = Z.of_string_base 16 "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F";
      a = Z.of_string_base 16 "0000000000000000000000000000000000000000000000000000000000000000";
      b = Z.of_string_base 16 "0000000000000000000000000000000000000000000000000000000000000007";
      g = Point (Z.of_string_base 16 "79BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798",
                 Z.of_string_base 16 "483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8");
      n = Z.of_string_base 16 "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141";
      h = Z.one
    }

end


(* let () = Random.self_init ()
 * 
 * (\*Generating random ints with a maximum length of decimal numbers*\)
 * let rec random_big_int bound =
 *   let max_size = String.length (Z.to_string bound) in
 *   let a = max (max_size/4) 1 in 
 *   let size = a + Random.int (max_size - a) in
 *   let big_int = Bytes.create size in
 *   let rand_str = Bytes.to_string @@
 *     Bytes.map (fun c -> let i = 48 + (Random.int 9) in Char.chr i) big_int 
 *   in
 *   let num = Z.(one + (of_string rand_str)) in
 *   match num < bound with
 *   | true -> num
 *   | false -> random_big_int bound *)

(* let generate_keypair curve = 
 *   let open PrimeField in
 *   let sk = random_big_int (get_n curve) in
 *   create_keypair curve sk *)


let is_pubkey_valid pk curve =
  let open PrimeField in
  match pk with
  | Infinity -> false
  | Point (pk_x, pk_y) -> 
    let curve_p = get_field curve in
    let verify_range (num : Z.t) (lowbound : Z.t) (upbound : Z.t) =
      (Z.(num >= lowbound) && Z.(num <= upbound)) in
    if (verify_range pk_x Z.zero Z.(curve_p - one)) &&
       (verify_range pk_y Z.zero Z.(curve_p - one)) &&
       is_point pk curve && 
       (multiply_point pk (get_n curve) curve) = Infinity
    then
      true
    else false

let create_keypair curve sk =
  let open PrimeField in
  let pk = multiply_point (get_g curve) sk curve in
  if (is_pubkey_valid pk curve) then
    (sk, pk)
  else
    raise Error


let append_checksum s = s ^ String.sub (s |> Crypto.sha256 |> Crypto.sha256) 0 8

let encode_btc_version version body =
  (Hex_ext.init (append_checksum (version ^ body)) |> Hex_ext.to_base58)


type keytype =
  | SEC
  | PUB

let hex_of_keytype = function
  | SEC ->
      "0488ADE4"
  | PUB ->
      "0488B21E"

let serialize_sk seed keytype depth parent_fingerprint child_number =
  let chaincode = String.sub seed 64 64 in
  let key = String.sub seed 0 64 in
  let xx = if keytype = SEC then "00" else "" in
  append_checksum ((hex_of_keytype keytype) ^ "00" ^ parent_fingerprint ^ child_number ^ chaincode ^ xx ^ key)
  |> Hex_ext.init |> Hex_ext.to_base58


let test () =
  let curve = PrimeField.secp256k1 in
  let (sk, pk) = create_keypair curve (Z.of_string "0x18e14a7b6a307f426a94f8114701e7c8e774e7f9a47e2c2035db29a206321725") in
  let p1 = match pk with
    | Infinity -> Z.zero
    | Point (x,y) -> x in
  let p2 = match pk with
    | Infinity -> Z.zero
    | Point (x,y) -> y in
  let p = if Z.is_even p2 then "02" ^ Z_ext.to_hex p1
    else "03" ^ Z_ext.to_hex p1 in
  let sha = Crypto.sha256 p in
  let rmd = Crypto.ripemd160 sha in
  let addr = encode_btc_version "00" rmd in
  let p2sh_p2wpkh = encode_btc_version "05" "86762607e8fe87c0c37740cddee880988b9455b2" in
  let seed = Crypto.hmac512 "Bitcoin seed" "8af20073a6b8eb105e51ad3641a96ec548b95e382479043d89b69e258b285fb4b7c61de97e3a30ec6cd105b0f8d7cd8a60d6bb43e691946e9b99f81e9ff85a4d" in
  let chaincode = Hex_ext.init (String.sub seed 64 64) in
  let master_seckey = Hex_ext.init (String.sub seed 0 64) in
  let m = Z_ext.of_hex master_seckey in
  if m = Z.zero || m >= (PrimeField.get_n PrimeField.secp256k1) then raise InvalidSecretKey else

  let ser = serialize_sk seed SEC "00" "00000000" "00000000" in
  
  Printf.printf
    "sk: 0x%s \np1: 0x%s \np2: 0x%s\np:  0x%s\nsha256: %s\nrmd160: %s\naddr:     %s\np2sh_p2wpkh: %s \
     \nseed: %s\nm_seckey:  %s\nchaincode: %s\nserialized: %s\nmust be:    xprv9s21ZrQH143K26fir2qf3bMDWw5gTTVkyXB54r16wsTLSeKEM557YiafiGe2ceqsZu9vEMEv53jYcK4f7iz5T9r1dHJ4krBERNTaykcYGaR\n\n" 
    (Z_ext.to_hex sk) (Z_ext.to_hex p1) (Z_ext.to_hex p2) p sha rmd addr p2sh_p2wpkh
     seed (Hex.show master_seckey) (Hex.show chaincode) ser



