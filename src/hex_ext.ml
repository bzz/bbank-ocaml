exception Invalid_base58_character

let alphabet = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"

let alphabet_values =
  let a = Array.make 256 (-1) in
  String.iteri (fun i c -> Array.unsafe_set a (Char.code c) i) alphabet ;
  a

let convert inp from_base to_base =
  let zero = Char.unsafe_chr 0 in
  let inp_len = Bytes.length inp in
  let inp_beg =
    let rec aux = function
      | i when i = inp_len || Bytes.get inp i <> zero ->
          i
      | i ->
          aux (i + 1)
    in
    aux 0
  in
  let buf_len =
    let inp_len = float_of_int inp_len in
    let from_base = float_of_int from_base in
    let to_base = float_of_int to_base in
    int_of_float @@ (1. +. (inp_len *. log from_base /. log to_base))
  in
  let buf = Bytes.make buf_len zero in
  let buf_last_index = buf_len - 1 in
  let carry = ref 0 in
  let buf_end = ref buf_last_index in
  for inp_i = inp_beg to inp_len - 1 do
    carry := Char.code (Bytes.unsafe_get inp inp_i) ;
    let rec iter = function
      | buf_i when buf_i > !buf_end || !carry <> 0 ->
          carry :=
            !carry + (from_base * (Bytes.unsafe_get buf buf_i |> Char.code)) ;
          Bytes.unsafe_set buf buf_i (Char.unsafe_chr (!carry mod to_base)) ;
          carry := !carry / to_base ;
          iter (buf_i - 1)
      | buf_end ->
          buf_end
    in
    buf_end := iter buf_last_index
  done ;
  let buf_written_len = buf_len - !buf_end - 1 in
  let out_len = inp_beg + buf_written_len in
  let out = Bytes.create out_len in
  Bytes.fill out 0 inp_beg zero ;
  Bytes.blit buf (!buf_end + 1) out inp_beg buf_written_len ;
  out

let to_base58 hex =
  let chr i s = String.unsafe_get s i in
  let b58 = convert (Hex.to_bytes hex) 256 58 in
  Bytes.map (fun c -> chr (Char.code c) alphabet) b58 |> Bytes.to_string

let of_base58 str =
  let value c alphabet_values =
    match Array.unsafe_get alphabet_values (Char.code c) with
    | -1 ->
        raise Invalid_base58_character
    | i ->
        i
  in
  let bin =
    Bytes.map
      (fun c -> Char.unsafe_chr (value c alphabet_values))
      (Bytes.of_string str)
  in
  convert bin 58 256 |> Hex.of_bytes

let init hex = Hex.of_cstruct (Cstruct.of_hex hex)
let ( ^^ ) a b = Hex.of_string (Hex.to_string a ^ Hex.to_string b)
let concat l = l |> List.map Hex.to_string |> String.concat "" |> Hex.of_string
