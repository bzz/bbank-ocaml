type t =
  | HEX
  | BASE58

let string_of_alphabet = function
  | HEX ->
      "0123456789abcdef"
  | BASE58 ->
      "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"

let encode alphabet a =
  let alphabet_str = string_of_alphabet alphabet in
  let base = Z.of_int (String.length alphabet_str) in
  let rec go a acc =
    let c = Char.escaped alphabet_str.[Z.to_int (Z.rem a base)] in
    if a < base then c ^ acc else go (Z.div a base) (c ^ acc)
  in
  let out = go a "" in
  if alphabet = HEX && String.length out mod 2 = 1 then "0" ^ out else out

let decode alphabet s =
  let alphabet_str = string_of_alphabet alphabet in
  let explode s =
    let rec exp i l = if i < 0 then l else exp (i - 1) (s.[i] :: l) in
    exp (String.length s - 1) []
  in
  let base = Z.of_int (String.length alphabet_str) in
  let ind = String.index alphabet_str in
  let rec apply cou acc zz =
    match zz with
    | [] ->
        acc
    | z :: zs ->
        apply (cou + 1) (Z.add (Z.mul base acc) (Z.of_int (ind z))) zs
  in
  apply 0 Z.zero (explode s)

let to_hex = encode HEX
let of_hex s = Z.of_string (String.concat "" ["0x"; Hex.show s])
