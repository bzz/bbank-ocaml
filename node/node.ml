open Bbank

let f () = ()

let show state =
  Printf.printf "AAAA\n" ;
  Lwt.return_unit

let add a state = Lwt.return_unit

let cmds =
  Cmdtui.(
    commands ~help:Cmdtui_lambda_term.display_help
      [("show", ("shows", const show)); ("add", ("adds", const add $ float))])

(* let () = Cmdtui_lambda_term.run ~prompt:"test" cmds () *)

let () = Ecc.test ()
